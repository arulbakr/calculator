#Calculator application

Implemented using .Net Console application

Goals:

BODMAS implemented
Added special functions like sqrt, pow and gauss function
Implemented with OOP and design pattern, so now, application is extensible for future changes
Tools used:

VS2012
Nunit 3.6 for Unit testing
Application setup:

Running exe is enough to launch the application.
Running application through VS2012