﻿using System.Collections.Generic;


namespace Media.Calc.Core
{
    public class DivideCommand : ICalcCommand
    {
        public double Execute(IDictionary<string, double> operands)
        {
            return operands["left"]/operands["right"];
        }
    }
}