﻿using System;
using System.Collections.Generic;


namespace Media.Calc.Core
{
    public class PowerCommand : ICalcCommand
    {
        public double Execute(IDictionary<string, double> operands)
        {
            return Math.Pow(operands["left"], operands["right"]);
        }
    }
}