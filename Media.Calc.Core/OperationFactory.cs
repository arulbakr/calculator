﻿
namespace Media.Calc.Core
{
    public class OperationFactory
    {
        public static ICalcCommand GetOperationCommand(string command)
        {
            ICalcCommand calcCommand = null;
            switch (command)
            {
                case "+":
                    calcCommand = new AddCommand();
                    break;
                case "-":
                    calcCommand = new SubtractCommand();
                    break;
                case "*":
                    calcCommand = new MultiplyCommand();
                    break;
                case "/":
                    calcCommand = new DivideCommand();
                    break;
                case "^":
                    calcCommand = new PowerCommand();
                    break;
                case "s":
                    calcCommand = new SquareRootCommand();
                    break;
                case "g":
                    calcCommand = new GaussCommand();
                    break;
            }
            return calcCommand;
        }
    }
}