﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Media.Calc.Core
{
    public class SquareRootCommand : ICalcCommand
    {
        public double Execute(IDictionary<string, double> operands)
        {
            return Math.Sqrt(operands.First().Value);
        }
    }
}