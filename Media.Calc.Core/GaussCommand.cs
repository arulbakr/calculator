﻿using System;
using System.Collections.Generic;


namespace Media.Calc.Core
{
    public class GaussCommand : ICalcCommand
    {
        public double Execute(IDictionary<string, double> operands)
        {
            double gaussValue = 0;
            if (operands.Count == 4)
            {
                double nomination = Math.Pow((operands["left"] - operands["b"]), 2); //x
                double denomination = 2 * Math.Pow(operands["right"], 2); //c
                gaussValue = operands["a"] * Math.Exp(-(nomination / denomination));
            }
            return gaussValue;
        }
    }
}