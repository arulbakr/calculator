﻿using System.Collections.Generic;

namespace Media.Calc.Core
{
    public interface ICalcCommand
    {
        /// <summary>
        /// Method executes given command
        /// </summary>
        /// <returns>result in double type</returns>
        double Execute(IDictionary<string, double> operands);
    }
}