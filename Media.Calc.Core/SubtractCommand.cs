﻿using System.Collections.Generic;


namespace Media.Calc.Core
{
    public class SubtractCommand : ICalcCommand
    {
        public double Execute(IDictionary<string, double> operands)
        {
            return operands["left"] - operands["right"];
        }
    }
}