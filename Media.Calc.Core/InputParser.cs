﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace Media.Calc.Core
{
    public class InputParser
    {
        static bool _isGauss;
        public static double Evaluate(string expr)
        {
            Stack<string> stack = new Stack<string>();
            string value = String.Empty;
            for (int i = 0; i < expr.Length; i++)
            {
                String s = expr.Substring(i, 1);
                char chr = s.ToCharArray()[0];
                if (!char.IsDigit(chr) && chr != '.' && value != "")
                {
                    stack.Push(value);
                    value = String.Empty;
                }
                if (s.Equals("("))
                {
                    string innerExp = "";
                    i++; //Fetch Next Character
                    int bracketCount = 0;
                    for (; i < expr.Length; i++)
                    {
                        s = expr.Substring(i, 1);
                        if (s.Equals("("))
                            bracketCount++;
                        if (s.Equals("s") || s.Equals("g") || s.Equals(","))
                        {
                            innerExp += s;
                            continue;
                        }
                        if (s.Equals(")"))
                            if (bracketCount == 0)
                                //continue;
                                break;
                            else
                                bracketCount--;
                        innerExp += s;
                    }
                    stack.Push(Evaluate(innerExp).ToString(CultureInfo.InvariantCulture));
                }
                else if (s.Equals("+") || s.Equals("-") || s.Equals("*") || s.Equals("/") || s.Contains("s") || s.Equals("^")) stack.Push(s);
                else if (s.Contains("g"))
                {
                    _isGauss = true;
                    stack.Push(s); }
                else if (s.Equals(")") || s.Equals(",")){}
                else if (char.IsDigit(chr) || chr == '.')
                {
                    value += s;
                    if (value.Split('.').Length > 2)
                        throw new Exception("Invalid decimal.");
                    if (i == (expr.Length - 1))
                        stack.Push(value);
                }
                else
                    throw new Exception("Invalid character.");
            }
            while ((stack.Contains("s") && stack.Count == 2) || (stack.Contains("g") || stack.Count >= 3))
            {
                IDictionary<string, double> operands = new Dictionary<string, double>();

                operands.Add("right", Convert.ToDouble(stack.Pop()));

                string op = _isGauss ? "g" : stack.Pop();
                if (stack.Count > 0)
                {
                    operands.Add("left", Convert.ToDouble(stack.Pop()));
                }
                if (stack.Count > 0 && op == "g")
                {
                    operands.Add("b", Convert.ToDouble(stack.Pop()));
                    operands.Add("a", Convert.ToDouble(stack.Pop()));
                }

                ICalcCommand calcCommand = OperationFactory.GetOperationCommand(op);
                if (calcCommand.GetType() == typeof(GaussCommand))
                {
                    _isGauss = false;
                }
                double result = calcCommand.Execute(operands);

                stack.Push(result.ToString(CultureInfo.InvariantCulture));
            }
            return Convert.ToDouble(stack.Pop());
        }
    }
}