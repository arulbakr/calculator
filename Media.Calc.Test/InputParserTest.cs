﻿using Media.Calc.Core;
using NUnit.Framework;

namespace Media.Calc.Test
{
    [TestFixture]
    public class InputParserTest
    {
        [Test]
        public void TestEvaluate1()
        {
            double result = InputParser.Evaluate("(1+3+(2+(100/5)+10))");
            Assert.That(result == 36);
        }

        [Test]
        public void TestEvaluate2()
        {
            double result = InputParser.Evaluate("(((10+5)+(5+6)+(10/5)+(2/1)))");
            Assert.That(result == 30);
        }

        [Test]
        public void TestEvaluate3()
        {
            double result = InputParser.Evaluate("(10+5-(6/3)*2)");
            Assert.That(result == 11);
        }

        [Test]
        public void TestEvaluate4()
        {
            double result = InputParser.Evaluate("(1+(3+(4+5)+2)+1)");
            Assert.That(result == 16);
        }

        [Test]
        public void TestEvaluate5()
        {
            double result = InputParser.Evaluate("2+(100/5)+10");
            Assert.That(result == 32);
        }

        [Test]
        public void TestEvaluate6()
        {
            double result = InputParser.Evaluate("((2.5+10)/5)+2.5");
            Assert.That(result == 5);
        }

        [Test]
        public void TestEvaluate7()
        {
            double result = InputParser.Evaluate("(2.5+10)/5+2.5");
            Assert.That(result == 1.66666666666667);
        }

        [Test]
        public void TestEvaluate8()
        {
            double result = InputParser.Evaluate("(10+5-(s(9))^2)");
            Assert.That(result == 6);
        }

        [Test]
        public void TestEvaluate9()
        {
            double result = InputParser.Evaluate("1+16.5/(2+(g(5,1,0,1)))");
            Assert.That(result == 9.25);
        }

        [Test]
        public void TestEvaluate10()
        {
            double result = InputParser.Evaluate("1+(g(5,1,0,1))+(g(5,1,0,1)))");
            Assert.That(result == 1);
        }

        [Test]
        public void TestEvaluate11()
        {
            double result = InputParser.Evaluate("(1)+(g(5,1,0,1))+(g(5,1,0,1)))");
            Assert.That(result == 1);
        }

        [Test]
        public void TestEvaluate12()
        {
            double result = InputParser.Evaluate("1+(g(5,1,10,1))+(g(5,1,0,1)))");
            Assert.That(result == 1);
        }

        [Test]
        public void TestEvaluate13()
        {
            double result = InputParser.Evaluate("1+(g(5,1,10,1))+(g(10,1,20,1)))");
            Assert.That(result == 1);
        }

        [Test]
        public void TestEvaluate14()
        {
            double result = InputParser.Evaluate("s(81)+(g(5,1,10,1))+(g(10,1,20,1)))");
            Assert.That(result == 9);
        }

        [Test]
        public void TestEvaluate15()
        {
            double result = InputParser.Evaluate("((2^6)+(s(81)+(g(5,1,10,1))+(g(10,1,20,1)))))");
            Assert.That(result == 73);
        }
    }
}